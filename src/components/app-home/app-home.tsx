import { Component } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css'
})
export class AppHome {

  hostData() {
    return {
      class: {'ion-page': true}
    };
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color='primary'>
          <ion-buttons slot='start'>
            <ion-icon slot="icon-only" name='logo-ionic'/>
          </ion-buttons>
          <ion-title>BTP</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content padding>

        <ion-grid>
          <ion-row>
            <ion-col>
              <ion-item>
                <ion-label position="floating">Envie sua sugestão</ion-label>
                <ion-textarea clear-on-edit="true"></ion-textarea>
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size="8">
            </ion-col>
            <ion-col size="4">
              <ion-button shape="round" class="right">Enviar</ion-button>
            </ion-col>
          </ion-row>
        </ion-grid>

        <ion-grid>
          <ion-row>
            <ion-col size="2" class="thumbs-box"></ion-col>
            <ion-col size="10" padding><h2 class="feed-day-divisor">Hoje</h2></ion-col>
          </ion-row>
          <ion-row>
            <ion-col size="2" class="thumbs-box">
              <ion-row>
                <ion-button fill="clear" size="large" color="success" class="thumbs-buttons">
                  <ion-icon slot="icon-only" name="md-thumbs-up"></ion-icon>
                </ion-button>
              </ion-row>
              <ion-row>
                <ion-col>
                  <center>5</center>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-button fill="clear" size="large" color="danger" class="thumbs-buttons">
                  <ion-icon slot="icon-only" name="md-thumbs-down"></ion-icon>
                </ion-button>
              </ion-row>
            </ion-col>
            <ion-col size="10" padding>
              <div class="post-box">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sodales mauris nunc, sit amet malesuada turpis convallis eget. Sed eget orci eu purus pulvinar semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sodales mauris nunc, sit amet malesuada turpis convallis eget. Sed eget orci eu purus pulvinar semper.
                </p>
                <div class="post-icons">
                  <ion-icon slot="icon-only" name="md-attach" class="attach-icon"></ion-icon>
                </div>
              </div>
              <ion-row>
                <ion-col size="8">
                  <div class="post-footer">
                    Categoria | 14h35
                  </div>
                </ion-col>
                <ion-col size="4">
                  <ion-button shape="round" fill="outline" class="right">Responder</ion-button>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </ion-grid>
        
      </ion-content>
    ];
  }
}
